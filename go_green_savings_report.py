#Course code 	   : SODV1101
#Term/Year 	     : fall2017
#Assignment code : PA6
#Author 		 : Nico Beekhuijs
#BVC Username 	  : n.beekhuijs948
#Date created 	  : 2017-12-02
#Description 	   : source code

# program purpose
"""this program reports energy savings"""

def main():
    """this is the main function"""
    global billData
    billData = []
    #billData = [[1,123,12],[2,324,2],[6,56,4],[5,654,45],[4,3455,234],[3,675,56],[7,123,32],[8,213,2],[9,67,23],[10,23,234],[11,233,123]]

    print("Green Energy Program:")
    #data input
    while(len(billData) < 12):
        monthNumber = int(getMonthInput())
        if(monthNumber < 0):
            break
        #correct month input, lets ask for bill values
        print("NOT GREEN energy costs for " + month2Text(monthNumber))
        billBefore = getBillInput()
        if(billBefore < 0):
            break
        print("GONE GREEN energy costs for " + month2Text(monthNumber))
        billAfter  = getBillInput()
        if(billAfter < 0):
            break
        #store the data
        billSave(monthNumber, billBefore, billAfter)
        print("-------------------------------------------------")
    #all months entered lets process
    print("Where would you like to output the results?")
    print("S=Screen, F=File, B=Both, N=None: ")
    # check if input is valid...

    #create the report
    createReport()



def padSpaces(inputNumber):
    """this function ads padding spaces to the left side of the values, so they all
        line up in the output report"""
    paddedText = ""
    for  numberOfSpaces in range(0, 5 - len(str(inputNumber))):
        paddedText = " "+paddedText
    paddedText = paddedText+str(inputNumber)
    return paddedText


def array2Screen(inputArray):
    """this function will output the data to screen"""
    for entryNumber in range(0, len(inputArray)):
        print(inputArray[entryNumber])


def array2File(inputArray):
    """this function will output the data to a file"""
    from time import strftime, localtime
    fileName = "pf_pa6_"+strftime("%Y%m%d_%H%M%S", localtime())+".txt"
    file = open(fileName,"w")

    for entryNumber in range(0, len(inputArray)):
        file.write(inputArray[entryNumber]+"\n")
    file.close()


def createReport():
    """this function creates the report"""
    global billData
    outputArray = []
    highVal = []
    lowVal = []
    totalGreen = 0
    totalNotGreen = 0
    totalSavings = 0
    notGreenSpace = ""
    goneGreenSpace = ""
    savingsSpace = ""
    #months should be in decending order, so lets sort
    billData = sortArray(billData)
    #find the highest and lowest entries
    highVal, lowVal = findHighLow(billData)

    #report header
    outputArray.append("------------------------------------------------- ")
    outputArray.append("                      SAVINGS                     ")
    outputArray.append("--------------------------------------------------")
    outputArray.append("SAVINGS     NOT GREEN      GONE GREEN  MONTH      ")
    outputArray.append("--------------------------------------------------")
    #next we need one line for each month
    for entryNumber in range(0, len(billData)):
        notGreenSpace = "  "
        goneGreenSpace = "  "
        savingsSpace = "  "
        monthText = month2Text(billData[entryNumber][0])
        goneGreen = padSpaces(billData[entryNumber][2])
        notGreen = padSpaces(billData[entryNumber][1])
        monthSavings = padSpaces(billData[entryNumber][1] - billData[entryNumber][2])
        #check if this line needs a high/low indicator
        if(billData[entryNumber][1] == billData[highVal[0]][1]):
            #not green high
            notGreenSpace = " h"
        if(billData[entryNumber][1] == billData[lowVal[0]][1]):
            #not green low
            notGreenSpace = " l"
        if(billData[entryNumber][2] == billData[highVal[1]][2]):
            #gone green high
            goneGreenSpace = " h"
        if(billData[entryNumber][2] == billData[lowVal[1]][2]):
            #gone green low
            goneGreenSpace = " l"
        if(billData[entryNumber][1] - billData[entryNumber][2] == billData[highVal[2]][1] - billData[highVal[2]][2]):
            #savings high
            savingsSpace = " h"
        if(billData[entryNumber][1] - billData[entryNumber][2] == billData[lowVal[2]][1] - billData[lowVal[2]][2]):
            #savings low
            savingsSpace = " l"
        
        
        outputArray.append("$"+str(monthSavings)+savingsSpace+"    $"+str(notGreen)+notGreenSpace+"       $"+str(goneGreen)+goneGreenSpace+"    "+monthText)
        totalGreen = totalGreen + billData[entryNumber][2]
        totalNotGreen = totalNotGreen + billData[entryNumber][1]
        totalSavings = totalSavings + (billData[entryNumber][1] - billData[entryNumber][2])
    outputArray.append("----------  ---------   -----------    ---------  ")
    #calculate averages
    goneGreen = padSpaces(totalGreen // len(billData))
    notGreen = padSpaces(totalNotGreen // len(billData))
    monthSavings = padSpaces(totalSavings // len(billData))
    outputArray.append("$"+str(monthSavings)+"      $"+str(notGreen)+"         $"+str(goneGreen)+"      "+"Average")
    
    array2Screen(outputArray)
    array2File(outputArray)
    
    
def findHighLow(inputArray):
    """this function finds the highest and lowest values in the array"""
    highVal = [0,0,0]
    lowVal = [0,0,0]
    for entryNumber in range(0, len(inputArray)):
        #highest value first field
        if (inputArray[entryNumber][1] > inputArray[highVal[0]][1]):
            highVal[0] = entryNumber
        #lowest value first field
        elif (inputArray[entryNumber][1] < inputArray[lowVal[0]][1]):
            lowVal[0] = entryNumber
        #highest value second field
        if (inputArray[entryNumber][2] > inputArray[highVal[1]][2]):
            highVal[1] = entryNumber
        #lowest value second field
        elif (inputArray[entryNumber][2] < inputArray[lowVal[1]][2]):
            lowVal[1] = entryNumber
        #highest value savings (first - second)
        if ((inputArray[entryNumber][1] - inputArray[entryNumber][2]) > (inputArray[highVal[2]][1] - inputArray[highVal[2]][2])):
            highVal[2] = entryNumber
        #lowest value savings (first - second)
        elif ((inputArray[entryNumber][1] - inputArray[entryNumber][2]) < (inputArray[lowVal[2]][1] - inputArray[lowVal[2]][2])):
            lowVal[2] = entryNumber
    return highVal, lowVal


def sortArray(inputArray):
    """this function will sort the inputArray by moving the entry with
        the highest value to the outputArray. This process will be repeated
        untill the inputArray has no more entries"""
    returnArray = []
    while(len(inputArray)):
        highestValuePosition = 0
        for entryNumber in range(0, len(inputArray)):
            if (inputArray[entryNumber][0] > inputArray[highestValuePosition][0]):
                highestValuePosition = entryNumber
        returnArray.append(inputArray.pop(highestValuePosition))
    return(returnArray)

def billSave(monthNumber, billBefore, billAfter):
    """this function stores the bill values for the month into an array"""
    #lets check if the current entry already exists
    entryFound = 0
    for entryNumber in range(0, len(billData)):
        if(billData[entryNumber][0] == monthNumber):
            entryFound = 1
            break

    if (entryFound == 1):
        #overwrite
        billData[entryNumber] = [monthNumber, billBefore, billAfter]
    else:
        #new entry
        billData.append([monthNumber, billBefore, billAfter])

def getBillInput():
    """this function asks for the input of the bill amount"""
    wrongInput = 0
    while True:
        try:
            answerNum = int(input("Enter value --> "))
        except ValueError:
            print("Please​ ​enter​ ​a​ ​value​ ​between​ ​0​ ​and​ ​9999. ")
            wrongInput = wrongInput + 1
            if(wrongInput > 3):
                return -1
        else:
            if(answerNum < 0 or answerNum > 9999):
                print("Please​ ​enter​ ​a​ ​value​ ​between​ ​0​ ​and​ ​9999. ")
                wrongInput = wrongInput + 1
                if(wrongInput > 3):
                    return -1
            else:
                return answerNum


def text2Month(searchFor):
    """This function returns month number 1 - 12 depending on input text.
        If the input string can not uniquely identify a month, the return
        value will be -1. The function is case insensitive"""
    #If this function needs to be translated into another language, make sure to pad every entry with spaces
    #so that every entry has the same lenght. Change the entryLenght variable accordingly.
    allMonths = "January  February March    April    May      June     July     August   SeptemberOctober  November December "
    entryLenght = 9
    foundPos = -1
    
    teststring = allMonths.lower()
    searchFor = searchFor.lower()

    if(teststring.find(searchFor, teststring.find(searchFor)+1) == -1):
        foundPos = teststring.find(searchFor)
        if(foundPos >= 0):
            foundPos = foundPos//entryLenght + 1
    return(foundPos)

def month2Text(monthNumber):
    """This function returns the month name based on the month number passed"""
    monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    return(monthNames[monthNumber - 1])

def getMonthInput():
    """this function asks the user to input the month"""
    #monthInput = input("Enter Month number: ")

    while True:
        try:
            monthInput = input("Enter Month number: ")
            int(monthInput)
        except ValueError:
            #probably text, lets check if unique enough
            monthNumber = text2Month(monthInput)
            if(monthNumber < 0):
                #not good
                print("Please​ ​enter​ ​month​ ​number​ ​between​ ​1​ ​and​ ​12,​ ​or​ ​a​ ​more​ ​specific​ ​month name.")
                continue
            else:
                return monthNumber
        else:
            if (int(monthInput) > 12 or int(monthInput) < 1):
                #out of range
                if (int(monthInput) < 0):
                    return monthInput
                print("Please​ ​enter​ ​month​ ​number​ ​between​ ​1​ ​and​ ​12.")
                continue
            else:
                return monthInput


main()
